<?php
 /**
  * * Implements hook_entity_presave().
 */
function filefield_paths_entity_presave($entity, $type) {
  if (isset($entity->original)) {
    $field_types = _filefield_paths_get_field_types();
    $entity_info = entity_get_info($type);
    $bundle_name = !empty($entity_info['entity keys']['bundle']) ? $entity->{$entity_info['entity keys']['bundle']} : $type;
    // Go through $entity->original to see which fileifelds have already been placed in their paths
    if ($entity_info['fieldable']) {
      $entity->filefield_paths_processed = array();
      foreach (field_info_fields($type, $bundle_name) as $field) {
        if (in_array($field['type'], array_keys($field_types))) {
          $instance = field_info_instance($type, $field['field_name'], $bundle_name);
          $enabled = (isset($instance['settings']['filefield_paths_enabled']) && $instance['settings']['filefield_paths_enabled']) || !isset($instance['settings']['filefield_paths_enabled']);
          if ($enabled && isset($entity->{$field['field_name']})) {
            foreach ($entity->original->{$field['field_name']} as $langcode => &$deltas) {
              foreach ($deltas as $delta => &$file) {
                $entity->filefield_paths_processed[$file['fid']] = $file['fid'];
              }
            }
            // Check to see if this file already appears on another entity -- if so we wouldn't want to move it
            foreach ($entity->{$field['field_name']} as $langcode => &$deltas) {
              foreach ($deltas as $delta => &$file) {
                if (isset($entity->filefield_paths_processed[$file['fid']])) {
                  continue;
                }
                if ($file_loaded = file_load($file['fid'])) {
                  if (file_usage_list($file_loaded)) {
                    $entity->filefield_paths_processed[$file['fid']] = $file['fid'];
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
